# dots

```
git clone --bare https://gitlab.com/Pauldg/dots.git $HOME/.dotfiles

alias dots='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

dots checkout
```
